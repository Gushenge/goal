package com.gushenge.goal.activities

import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gushenge.atools.util.AView
import com.gushenge.goal.GoalApplication
import me.jessyan.autosize.AutoSizeCompat
import me.jessyan.autosize.internal.CustomAdapt

open class BaseActivity:AppCompatActivity(), CustomAdapt {
    override fun isBaseOnWidth(): Boolean {
        return true
    }

    override fun getSizeInDp(): Float {
        return 360.toFloat()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AView.setStatusBar(this,true,Color.WHITE)
        GoalApplication.instance.init(this)
    }

    override fun getResources(): Resources {
        val res = super.getResources()
        val config = Configuration()
        config.setToDefaults()
        res.updateConfiguration(config, res.displayMetrics)
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()))
        AutoSizeCompat.autoConvertDensity(super.getResources(), 360.toFloat(), true)
        return res
    }

    override fun onDestroy() {
        super.onDestroy()
        GoalApplication.instance.remove(this)
    }

}