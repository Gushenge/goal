package com.gushenge.goal.activities

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.gushenge.goal.GoalApplication
import com.gushenge.goal.R
import com.gushenge.goal.fragments.FunctionFragment
import com.gushenge.goal.fragments.MeFragment
import com.gushenge.goal.fragments.OverViewFragment
import com.gushenge.goal.uis.bottomNavigationView
import org.jetbrains.anko.*

class MainActivity : BaseActivity() {

    private var overViewFragment:OverViewFragment?=null
    private var meFragment:MeFragment?=null
    private var functionFragment:FunctionFragment?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        tabSelected(0)
    }

    private fun initView() {
        verticalLayout {
            frameLayout {
                id = R.id.frame
                backgroundColor =Color.WHITE
            }.lparams(matchParent, wrapContent){
                weight = 1f
            }
            bottomNavigationView {
                labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_AUTO
                backgroundColor = Color.WHITE
                inflateMenu(R.menu.navigation)
                itemIconSize = dip(20)
                setOnNavigationItemSelectedListener {
                    when(it.itemId){
                        R.id.overview->{
                            tabSelected(0)
                        }
                        R.id.function->{
                            tabSelected(1)
                        }
                        R.id.me->{
                            tabSelected(2)
                        }
                    }
                    true
                }
            }.lparams(matchParent, wrapContent)
        }
    }


    private fun tabSelected(index:Int){
        val manage = supportFragmentManager
        val transtion = manage.beginTransaction()
        hideFragment(transtion)
        when(index){
            0->{
                overViewFragment?.let {
                    transtion.show(it)
                }
                overViewFragment?:let {
                    overViewFragment = OverViewFragment()
                    transtion.add(R.id.frame,overViewFragment?:OverViewFragment())
                }
            }
            1->{
                functionFragment?.let {
                    transtion.show(it)
                }
                functionFragment?:let {
                    functionFragment = FunctionFragment()
                    transtion.add(R.id.frame,functionFragment?:FunctionFragment())
                }
            }
            2->{
                meFragment?.let {
                    transtion.show(it)
                }
                meFragment?:let {
                    meFragment = MeFragment()
                    transtion.add(R.id.frame,meFragment?:MeFragment())
                }
            }
        }
        transtion.commit()
    }
    private fun hideFragment(transtion:FragmentTransaction){
        overViewFragment?.let { transtion.hide(it) }
        functionFragment?.let { transtion.hide(it) }
        meFragment?.let { transtion.hide(it) }
    }
    override fun onBackPressed() {
        GoalApplication.instance.destory()
    }
}
