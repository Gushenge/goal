package com.gushenge.goal.activitys

import android.os.Bundle
import com.gushenge.atools.util.AView
import com.gushenge.goal.R
import com.gushenge.goal.activities.BaseActivity
import com.gushenge.goal.activities.MainActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.*

class LaunchActivity : BaseActivity(){
    lateinit var job:Job
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AView.fullScreen(this)
        initView()
        initTime()
    }

    private fun initTime() {
        job = GlobalScope.launch {
            delay(1000)
            startActivity<MainActivity>()
        }
    }

    private fun initView() {
        verticalLayout {
            relativeLayout {
                textView(R.string.app_name){
                    textSize = 32f
                    textColor = getColor(R.color.launch_title)
                }.lparams(wrapContent, wrapContent){
                    topMargin  = dip(66)
                    centerHorizontally()
                }
                textView(R.string.launch_desc){
                    textSize = 16f
                    textColor = getColor(R.color.launch_desc)
                }.lparams(wrapContent, wrapContent){
                    bottomMargin  = dip(66)
                    alignParentBottom()
                    centerHorizontally()
                }
            }.lparams(matchParent, matchParent)
        }
    }
}
