package com.gushenge.goal.uis

import android.view.ViewManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.jetbrains.anko.custom.ankoView


inline fun ViewManager.bottomNavigationView(init: BottomNavigationView.() -> Unit): BottomNavigationView {
    return ankoView({ BottomNavigationView(it) }, theme = 0, init = init)
}