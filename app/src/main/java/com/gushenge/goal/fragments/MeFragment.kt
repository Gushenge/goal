package com.gushenge.goal.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.verticalLayout

class MeFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return UI {
            verticalLayout {
                backgroundColor = Color.GRAY
            }
        }.view
    }
}