package com.gushenge.goal.fragments

import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import androidx.core.graphics.drawable.toDrawable
import androidx.fragment.app.Fragment
import com.gushenge.atools.ui.ArcButton
import com.gushenge.atools.ui.arcButton
import com.gushenge.goal.R
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.toast
import java.util.*

class OverViewFragment:BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UI {
            verticalLayout {
                backgroundColor = Color.WHITE
                verticalLayout {
                    fitsSystemWindows = true
                    arcButton{
                        backgroundColor = Color.WHITE
                        elevation = 50f
                        text = "${Calendar.getInstance().get(Calendar.MONTH)+1}月${Calendar.getInstance().get(Calendar.DAY_OF_MONTH)+1}日"
                        textColor = Color.BLUE
                        textSize = 15f
                        setRadius(dip(50).toFloat())
                    }.lparams(dip(250),dip(50)){
                        gravity = Gravity.CENTER_HORIZONTAL
                        topMargin = dip(20)
                    }

                }.lparams(matchParent, matchParent)
            }
        }.view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}