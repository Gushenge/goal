package com.gushenge.goal.fragments

import androidx.fragment.app.Fragment
import me.jessyan.autosize.internal.CustomAdapt

open class BaseFragment:Fragment(),CustomAdapt{
    override fun isBaseOnWidth(): Boolean {
        return true
    }

    override fun getSizeInDp(): Float {
        return 360f
    }
}