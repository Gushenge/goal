package com.gushenge.goal

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import me.jessyan.autosize.AutoSizeConfig

class GoalApplication:Application() {
    val beans = mutableListOf<AppCompatActivity>()
    private val context = this@GoalApplication
    override fun onCreate() {
        super.onCreate()
        AutoSizeConfig.getInstance().setCustomFragment(true).isBaseOnWidth = true
        instance = this
    }

    fun init(context:AppCompatActivity){
        beans.add(context)
    }
    fun remove(context:AppCompatActivity){
        beans.remove(context)
    }
    fun destory(){
        for(i in beans){
            i.finish()
        }
    }

    companion object{
        lateinit var instance: GoalApplication
            private set
    }
}